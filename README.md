# ZipPay API

Build an API to manage Zip Pay users and account

## Table of Contents
  * [Deploying the application](#deployingTheApplication)
  * [Accessing the application](#accessingTheApplication)
  * [Examples](#example)
  * [Version](#version)
  * [Contribution](#contribution)
  * [TO DO](#todo)



## Running the application <a id="deployingTheApplication"></a>

Zip pay API can be launched using a single command.
```sh
./build.sh
```
#### Prerequisites for running the application 

 - Active internet connection.
 - Docker and Docker Compose

The script build.sh will assemble the project and deploy components in docker container.
Three docker containers will be up and running once build is completed successfully.

- Zip Server Container
- MySql Server
- PhpMyAdmin as MySql client

## Accessing the application <a id="accessingTheApplication"></a>

- Zip Server:
   
  Zip server is listening to the port 8080.
  ```
    http://localhost:8080/
  ```
- MySql Server:

  MySql server is listening to the port 3306
  ```
    127.0.0.1:3306
  ```
- PhpMyAdmin Server:

  PhpMyAdmin server is listening to the port 9080
  ```
    http://localhost:9080/index.php 
  ```
  
  Zip Server API
  
  | Plugin | End point | Method |
  | ------ | ------ | ------ |
  | Create User | http://localhost:8080/user | POST |
  | List User | http://localhost:8080/users?page=0&count=3' | GET |
  | Get User | http://localhost:8080/user/:id | GET |
  | Create Account | http://localhost:8080/account | POST|
  | List Account | http://localhost:8080/accounts?page=0&count=3 | GET |
  
## Example <a id="example"></a>
  - Create User
    ```sh
      curl -i -X POST http://localhost:8080/user -d '{"name":"test","email":"sample@test.com","monthlySalary":6000, "monthlyExpense": 7000}'
    ```
  - List User
    ```
      curl -i -X GET 'http://localhost:8080/users?page=0&count=3'
    ```
  - Get User
    ```
      curl -i -X GET 'http://localhost:8080/user/7'
    ```
  - Create Account
    ```
      curl -i -X POST http://localhost:8080/account -d '{"userId": 8,"credit":1000}'
    ```
  - List Account
    ```
      curl -i -X GET 'http://localhost:8080/accounts?page=0&count=3'
    ```
## Version <a id="version"></a>
Version 1.0.0

## Contribution <a id="contribution"></a>
> Author : Sarath Sasikumar
>
> email  : sarathshasikumar@gmail.com

## TODO <a id="todo"></a>
  - Develop client application for endpoint.
  - Write functional test case for each API. 

License
----
MIT



































