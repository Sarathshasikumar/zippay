# Clean the target directory of the application.
./sbt clean

# Compile the source code before assembly.
./sbt compile

if [ $? -ne 0 ];then
    echo "Compilation failed due to some issue."
    exit 1
fi

# Assemble the package as fat jar
./sbt assembly

if [ $? -ne 0 ];then
    echo "Assembly artifact failed"
    exit 1
fi

# Build or rebuild services
docker-compose build

if [ $? -ne 0 ];then
    echo "docker-compose failed"
    exit 1
fi

# Run docker-compose up and Compose starts and runs your entire app.
docker-compose up -d

# docker-compose down -v Use this command to stop the docker containers and clean up.