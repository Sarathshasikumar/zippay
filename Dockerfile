# Dockerfile References: https://docs.docker.com/engine/reference/builder/

# Start from the openjdk base image.
From openjdk:8

# Copy the compiled jar to docker container.
COPY ./target/scala-2.12/zip-server.jar zip-server.jar

# install the netcat in the docker to check the mysql sql status before triggering the application.
RUN apt-get update && apt-get install -y netcat

# Copy entry point script of the application.
COPY ./docker-entrypoint.sh /docker-entrypoint.sh

# Give permission to the application.
RUN chmod +x /docker-entrypoint.sh

# Define entry point of the application.
ENTRYPOINT [ "/docker-entrypoint.sh" ]