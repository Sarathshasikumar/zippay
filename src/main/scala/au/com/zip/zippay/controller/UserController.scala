package au.com.zip.zippay.controller

import com.twitter.finagle.mysql._
import com.twitter.util.Future

import io.circe.generic.auto._
import io.finch._
import io.finch.circe._

import au.com.zip.zippay.model.{Page, User}
import au.com.zip.zippay.repository.{UserRepository, UserRepositoryImpl}

class UserController(val userRepository: UserRepository)(implicit val client: Client) {

  val page: Endpoint[Page] = (param("page").as[Int] :: param("count").as[Int]).as[Page]

  /**
   * The endpoint to create a user in zip store.
   * Http Method: POST
   * curl -i -X POST http://localhost:8080/user -d '{"name":"test","email":"sample@test.com","monthlySalary":6000, "monthlyExpense": 7000}'
   */
  private val createUser: Endpoint[User] = post("user" :: jsonBody[User]) { user: User =>
    (for {
      emailU <- userRepository.getByEmail(user.email)
      _      <- emailU match {
                  case Some(user) => Future.exception(new IllegalArgumentException("User with same email id already exist."))
                  case None       => Future.None
                }
      userId <- userRepository.create(user)
      user   <- userRepository.getById(userId)
    } yield user) map {
      case Some(user) => Ok(user)
      case _ => NotFound(new Exception("Cannot retrieve user from the database."))
    }
  } handle {
    case exception: IllegalArgumentException => Conflict(exception)
    case exception: Exception => InternalServerError(exception)
  }

  /**
   * Http endpoint to list all user available in the system.
   * Http Method: GET
   * curl -i -X GET 'http://localhost:8080/users?page=0&count=3'
   */
  private val listUser: Endpoint[Seq[User]] = get("users" :: page) { p: Page =>
    userRepository.list(p.page, p.count) map { users =>
      Ok(users)
    }
  } handle {
    case exception: Exception => InternalServerError(exception)
  }

  /**
   * Http endpoint to get a user using his user id
   * Http Method: GET
   * curl -i -X GET 'http://localhost:8080/user/7'
   */
  private val getUser: Endpoint[User] = get("user" :: path[Long]) { id: Long =>
    userRepository.getById(id) map {
      case Some(user) => Ok(user)
      case None => NotFound(new Exception("Cannot retrieve user from the database."))
    }
  } handle {
    case exception: Exception => InternalServerError(exception)
  }


  val endpoints = createUser :+: listUser :+: getUser
}

object UserController {

  def apply()(implicit client: Client): UserController = new UserController(UserRepositoryImpl)(client)
}
