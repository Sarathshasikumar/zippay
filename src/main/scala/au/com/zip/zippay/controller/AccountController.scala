package au.com.zip.zippay.controller

import com.twitter.finagle.mysql._
import com.twitter.util.Future

import io.circe.generic.auto._
import io.finch._
import io.finch.circe._

import au.com.zip.zippay.model.{Account, Page, User}
import au.com.zip.zippay.repository._

class AccountController(val accountRepository: AccountRepository, val userRepository: UserRepository)(implicit val client: Client) {

  val page: Endpoint[Page] = (param("page").as[Int] :: param("count").as[Int]).as[Page]

  /**
   * Http endpoint to create an account
   * Http Method: POST
   * curl -i -X POST http://localhost:8080/account -d '{"userId": 8,"credit":1000}'
   *
   */
  private val createAccount: Endpoint[Account] = post("account" :: jsonBody[Account]) { account: Account =>
    (for {
      userOpt   <- userRepository.getById(account.userId)
      user      <- userOpt match {
                      case Some(user) => Future.value(user)
                      case None       => Future.exception(new IllegalArgumentException("User not found, Invalid Id."))
                    }
      _         <- ((user.monthlySalary - user.monthlyExpense) >= 1000) match {
                      case true =>  Future.None
                      case false => Future.exception(new UnsupportedOperationException(
                        "Cannot create account, (monthly salary - monthly expenses) should have to be greater than 1000"))
                    }
      accountId <- accountRepository.create(account)
      account   <- accountRepository.getById(accountId)
    } yield account) map {
      case Some(account) => Ok(account)
      case _ => NotFound(new Exception("Cannot retrieve account from the database."))
    }
  } handle {
    case exception: IllegalArgumentException      => NotFound(exception)
    case exception: UnsupportedOperationException => PreconditionFailed(exception)
    case exception: Exception                     => InternalServerError(exception)
  }

  /**
   * Http endpoint to get all accounts in the system.
   * Http Method: GET
   * curl -i -X GET 'http://localhost:8080/accounts?page=0&count=3'
   */
  private val listAccount: Endpoint[Seq[Account]] = get("accounts" :: page) { p: Page =>
    accountRepository.list(p.page, p.count) map { accounts =>
      Ok(accounts)
    }
  } handle {
    case exception: Exception => InternalServerError(exception)
  }

  val endpoints = createAccount :+: listAccount
}

object AccountController {

  def apply()(implicit client: Client): AccountController = new AccountController(AccountRepositoryImpl, UserRepositoryImpl)(client)
}


