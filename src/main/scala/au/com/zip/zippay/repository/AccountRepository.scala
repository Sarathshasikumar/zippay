package au.com.zip.zippay.repository

import com.twitter.finagle.mysql.Client
import com.twitter.finagle.mysql._
import com.twitter.util.Future

import au.com.zip.zippay.model.Account

trait AccountRepository {

  /**
   * This function will create a new account for a user. A user can only be allowed to create an
   * account if his (monthly salary - monthly expenses) is greater than 1000$
   * @param account instance of an account.
   * @param client implicit instance of database client that help in firing query against database.
   * @return the auto increment unique id created for a new account.
   */
  def create(account: Account)(implicit client: Client): Future[Long]

  /**
   * This function will list all account stored in the system. User can limit the no of account
   * being returned by this function using the limit parameter.
   * @param page no of pages of account need to return.
   * @param limit no of account need be shown returned.
   * @param client implicit instance of database client that help in firing query against database.
   * @return a list of account in specified limit.
   */
  def list(page: Int = 0, limit: Int = 100)(implicit client: Client): Future[Seq[Account]]

  /**
   * This function will helps to get account based on the account id of that account.
   * @param accountId account id of a account.
   * @param client implicit instance of database client that help in firing query against database.
   * @return a account identified by the unique id.
   */
  def getById(accountId: Long)(implicit client: Client): Future[Option[Account]]
}
