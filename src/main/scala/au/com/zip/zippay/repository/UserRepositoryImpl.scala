package au.com.zip.zippay.repository

import com.twitter.util.Future
import com.twitter.finagle.mysql._

import au.com.zip.zippay.model.User

object UserRepositoryImpl extends UserRepository {

  /**
   * This function will store a user data in the database. The only constrain in creating in user is
   * the email id of the user should have to be unique.
   * @param user instance of [[User]] to store in the database.
   * @param client implicit instance of database client that help in firing query against database.
   * @return the auto increment unique id created for a new user.
   */
  override def create(user: User)(implicit client: Client): Future[Long] = {

    val ps = client.prepare("INSERT INTO users (name, email, monthly_salary, monthly_expense) VALUES (?, ?, ?, ?)")
    ps(user.name, user.email, user.monthlySalary, user.monthlyExpense) map { result =>
      result.asInstanceOf[OK].insertId
    }
  }

  /**
   * This function will get the user stored in the database. The function help to query user by batch which will
   * helps to reduce network traffic.
   * @param page indicates how much page of data need to return.
   * @param limit indicated the limit of query.
   * @param client implicit instance of database client that help in firing query against database.
   * @return a list of [[User]] in the specified limit. Please note increasing the limit will introduce network traffic.
    */
  override def list(page: Int = 0, limit: Int = 100)(implicit client: Client): Future[Seq[User]] = {

    client.select(s"SELECT * FROM users u WHERE u.del_flg = 0 ORDER BY u.id DESC LIMIT $page, $limit") { row =>
      val LongValue(id) = row("id").get
      val StringValue(name) = row("name").get
      val StringValue(email) = row("email").get
      val LongValue(monthlySalary) = row("monthly_salary").get
      val LongValue(monthlyExpense) = row("monthly_expense").get
      User(Some(id), name, email, monthlySalary, monthlyExpense)
    }
  }

  /**
   * This function will helps to get user based on the user id of that user.
   * @param userId user id of a user.
   * @param client implicit instance of database client that help in firing query against database.
   * @return a user identified by the unique id.
   */
  override def getById(userId: Long)(implicit client: Client): Future[Option[User]] = {

    val ps = client.prepare("SELECT * FROM users u WHERE u.id = ? AND u.del_flg = 0")
    ps(userId).map { result =>
      result.asInstanceOf[ResultSet].rows.map { row =>
        val LongValue(id) = row("id").get
        val StringValue(name) = row("name").get
        val StringValue(email) = row("email").get
        val LongValue(monthlySalary) = row("monthly_salary").get
        val LongValue(monthlyExpense) = row("monthly_expense").get
        User(Some(id), name, email, monthlySalary, monthlyExpense)
      }.headOption
    }
  }

  /**
   * This function will helps to get user based on the email address.
   * @param email email id of a user.
   * @param client implicit instance of database client that help in firing query against database.
   * @return a user identified by the unique id.
   */
  override def getByEmail(email: String)(implicit client: Client): Future[Option[User]] = {

    val ps = client.prepare("SELECT * FROM users u WHERE u.email = ? AND u.del_flg = 0")
    ps(email).map { result =>
      result.asInstanceOf[ResultSet].rows.map { row =>
        val LongValue(id) = row("id").get
        val StringValue(name) = row("name").get
        val StringValue(email) = row("email").get
        val LongValue(monthlySalary) = row("monthly_salary").get
        val LongValue(monthlyExpense) = row("monthly_expense").get
        User(Some(id), name, email, monthlySalary, monthlyExpense)
      }.headOption
    }
  }
}
