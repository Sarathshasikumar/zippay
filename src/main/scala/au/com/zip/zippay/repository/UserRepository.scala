package au.com.zip.zippay.repository

import com.twitter.finagle.mysql.Client
import com.twitter.finagle.mysql._
import com.twitter.util.Future

import au.com.zip.zippay.model.User

trait UserRepository {

  /**
   * This function will store a user data in the database. The only constrain in creating in user is
   * the email id of the user should have to be unique.
   * @param user instance of [[User]] to store in the database.
   * @param client implicit instance of database client that help in firing query against database.
   * @return the auto increment unique id created for a new user.
   */
  def create(user: User)(implicit client: Client): Future[Long]

  /**
   * This function will get the user stored in the database. The function help to query user by batch which will
   * helps to reduce network traffic.
   * @param page indicates how much page of data need to return.
   * @param limit indicated the limit of query.
   * @param client implicit instance of database client that help in firing query against database.
   * @return a list of [[User]] in the specified limit. Please note increasing the limit will introduce network traffic.
   */
  def list(page: Int = 0, limit: Int = 100)(implicit client: Client): Future[Seq[User]]

  /**
   * This function will helps to get user based on the user id of that user.
   * @param userId user id of a user.
   * @param client implicit instance of database client that help in firing query against database.
   * @return a user identified by the unique id.
   */
  def getById(userId: Long)(implicit client: Client): Future[Option[User]]

  /**
   * This function will helps to get user based on the email address.
   * @param email email id of a user.
   * @param client implicit instance of database client that help in firing query against database.
   * @return a user identified by the unique id.
   */
  def getByEmail(email: String)(implicit client: Client): Future[Option[User]]
}
