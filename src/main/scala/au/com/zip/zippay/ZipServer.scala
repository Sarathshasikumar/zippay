package au.com.zip.zippay

import com.twitter.finagle.mysql._
import com.twitter.finagle.{Http, ListeningServer}
import com.twitter.server.TwitterServer
import com.twitter.util.{Await, Future}

import io.finch._, io.circe._
import io.circe.generic.auto._
import io.finch.circe._

import au.com.zip.zippay.db.{DDL, DatabaseBuilder}
import au.com.zip.zippay.controller.{AccountController, UserController}

/**
 * This class is the entry point of the application.
 * The class register various endpoint services to expose outside.
 * It has a exception encoder that encode the exception to JSON message.
 */
object ZipServer extends TwitterServer {

  override def failfastOnFlagsNotParsed: Boolean = true

  implicit val dbClient: Client = DatabaseBuilder.getClient

  implicit val encodeException: Encoder[Exception] = Encoder.instance(e =>
    Json.obj("message" -> Json.fromString(e.getMessage)))

  lazy val userController: UserController = UserController()
  lazy val accountController: AccountController = AccountController()

  val zipEndpoints = userController.endpoints :+: accountController.endpoints

  lazy val apiServer: ListeningServer = Http.server.serve(":8080", zipEndpoints.toService)

  /**
   * This function is used to initialize the database with User table and account table.
   * @param client implicit client that hold a database connection.
   * @return the result of execution.
   */
  def createTables()(implicit client: Client): Future[Result] = {
    client.query(DDL.createUsersTable)
    client.query(DDL.createAccountsTable)
  }

  def main() {
    onExit{
      apiServer.close()
    }
    Await.result(createTables())
    Await.ready(apiServer)
  }
}