package au.com.zip.zippay.model

/**
 * Represent a object that help in paging list of user.
 * @param page the index that points a particular page.
 * @param count number of pages.
 */
case class Page(page: Int, count: Int)