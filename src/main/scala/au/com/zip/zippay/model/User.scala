package au.com.zip.zippay.model

/**
 * Represent a user in the system.
 * @param id unique id of user.
 * @param name name of the user.
 * @param email unique email id of a user.
 * @param monthlySalary monthly salary of a user.
 * @param monthlyExpense monthly expenses of a user.
 */
case class User(
  id: Option[Long] = None,
  name: String,
  email: String,
  monthlySalary: Long,
  monthlyExpense: Long)
