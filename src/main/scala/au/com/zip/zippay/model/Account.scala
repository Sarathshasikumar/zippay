package au.com.zip.zippay.model

/**
 * Represent an Account of a [[User]].
 * @param id unique id of a account.
 * @param userId user id of account holder.
 * @param credit credit allowed for a user.
 */
case class Account(
  id: Option[Long] = None,
  userId: Long,
  credit: Long)
