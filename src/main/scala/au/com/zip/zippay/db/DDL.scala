package au.com.zip.zippay.db

/**
  * Created by Sarath Sasikumar on 2019-11-28.
  * This class helps in initializing the database table while booting the application.
  */
object DDL {

  //User table DDL statement.
  val createUsersTable =
    """CREATE TABLE IF NOT EXISTS `users` (
       `id` bigint(20) NOT NULL AUTO_INCREMENT,
       `name` varchar(255) NOT NULL,
       `email` varchar(255) NOT NULL,
       `monthly_salary` bigint(20) NOT NULL,
       `monthly_expense` bigint(20) NOT NULL,
       `del_flg` tinyint(4) NOT NULL DEFAULT '0',
       `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
       `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY (`id`),
        UNIQUE KEY (`email`)
       ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8
    """
  //Account table DDL statement.
   val createAccountsTable =
     """CREATE TABLE IF NOT EXISTS `accounts` (
       `id` bigint(20) NOT NULL AUTO_INCREMENT,
       `user_id` bigint(20) NOT NULL,
       `credit` bigint(20) NOT NULL,
       `del_flg` tinyint(4) NOT NULL DEFAULT '0',
       `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
       `update_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`),
        UNIQUE KEY (`id`),
        INDEX user_ind (user_id),
        FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
       ) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8
    """
}
