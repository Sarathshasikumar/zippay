package au.com.zip.zippay.db

import java.net.InetSocketAddress

import com.twitter.finagle.Mysql
import com.twitter.finagle.mysql._
import com.typesafe.config._

/**
 * Created by Sarath Sasikumar on 2019-11-28.
 * This class reads the configuration file and initialize database connection with username
 * and password provided. The configuration file is placed inside resource folder.
 */
trait DatabaseBuilder {

  val conf: Config
  val host: String
  val port: Int
  val user: String
  val password: String
  val db: String

  lazy val url = new InetSocketAddress(host, port)

  def getClient: Client with Transactions
}

object DatabaseBuilder extends DatabaseBuilder {

  val conf: Config = ConfigFactory.load()
  val host: String = conf.getString("mysql.host")
  val port: Int = conf.getInt("mysql.port")
  val user: String = conf.getString("mysql.user")
  val password: String = conf.getString("mysql.password")
  val db: String = conf.getString("mysql.db")

  /**
   * Initializing database connection with username password
   * @return a mysql client instance to interact with database.
   */
  def getClient: Client with Transactions = Mysql.client
    .withCredentials(user, password)
    .withDatabase(db)
    .newRichClient("%s:%d".format(url.getHostName, url.getPort))
}
