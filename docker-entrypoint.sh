#!/bin/bash

# Before triggering the zip server application we need to wait for MySql server to
# intialize otherwise the zip server will fail to run.

echo "Checking the status of MySql server!"
while ! nc -z zip_sql_server 3306;
 do
   echo sleeping;
   sleep 1;
 done;
 echo "MySql server up and running."

echo "Starting zip server"
java -jar zip-server.jar
echo "Started zip server"