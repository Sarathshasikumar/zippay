name := "zip-pay"

organization := "zip"

version := "1.0"

scalaVersion := "2.12.4"

// Dependencies used by the application.
libraryDependencies ++= Seq(
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe" % "config" % "1.3.1",
  "com.twitter" %% "finagle-http" % "17.12.0",
  "com.twitter" %% "finagle-mysql" % "17.12.0",
  "com.twitter" %% "twitter-server" % "17.12.0",
  "com.github.finagle" %% "finch-core" % "0.16.0-M5",
  "com.github.finagle" %% "finch-circe" % "0.16.0-M5",
  "io.circe" %% "circe-generic" % "0.9.0-M2"
)

// Disable running testing while doing assembly
test in assembly := {}

// Define the final artifact name.
assemblyJarName in assembly := s"zip-server.jar"

// Define the entry point of the application.
mainClass in assembly := Some("au.com.zip.zippay.ZipServer")

assemblyMergeStrategy in assembly := {
  case PathList("reference.conf") => MergeStrategy.concat
  case PathList("META-INF", "MANIFEST.MF") => MergeStrategy.discard
  case _ => MergeStrategy.first
}
